﻿angular.module('SGA').directive('reportdate', function () {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {   //as variaveis que serão usadas no html para receber o dado 
            parameter: '='
        },
        templateUrl: "assets/templates/report/reportdate.html",
        controllerAs: 'ctrl',
        controller: function () {   //Na function eu adiciono se necessario os modulos($http, settings, $stateParameters) que vai precisar
            var ctrl = this
            this.today = function () {
                this.date = new Date();
                this.datafim = new Date();
                
            };
            this.showWeeks = false;
            this.today();
            this.open = function ($filter) {
                ctrl.popup.opened = true;
            };
            this.popup = {
                opened: false,
                
            };
        }
    }

});
