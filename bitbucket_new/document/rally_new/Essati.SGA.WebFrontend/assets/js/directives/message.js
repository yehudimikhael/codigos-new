﻿
angular.module('SGA').directive('message', ['messageService', 'prompt', function (messageService, prompt) {
    return {
        restrict: 'E',
        controllerAs: 'ctrl',
        scope: {},
        controller: ['$scope', function ($scope) {
            var ctrl = this;
            ctrl.msg = null;
            ctrl.delete = function () {
                messageService.delete();
                ctrl.msg = null;
                ctrl.log.length = 0;
            }
            $scope.$watch(
                function () {
                    ctrl.log = messageService.messages;
                    return ctrl.log.length;
                },
                function (newVal) {
                    ctrl.msg = ctrl.log;
                    if (ctrl.msg.length > 0) {
                        prompt({
                            title: ctrl.msg[0].titl,
                            message: ctrl.msg[0].text,
                            input: false,
                            buttons: [
                                    { label: 'OK', primary: true }
                            ]
                        }).then(function () { ctrl.delete(); }), function () { ctrl.delete() }
                    }
                })
        }]
    }
}])
