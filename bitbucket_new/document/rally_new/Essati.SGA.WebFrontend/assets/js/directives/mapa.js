﻿
angular.module('SGA').directive('sgaMap', function (searchService) {
    return {
        restric: 'E',
        scope: {},
        bindToController: {

            results: '=sgaResults',

        },
        templateUrl: "assets/js/directives/templates/mapa.html",
        controllerAs: 'ctrl',
        controller: ['$scope', 'leafletMarkerEvents', 'protectedareaService', 'leafletData',
    function ($scope, leafletMarkerEvents, protectedareaService, leafletData) {

        var ctrl = this;
        ctrl.markersMap = {};

        /*Foi utilizado para evitar erros com os marcadores 
         Ao atualizar os marcadores não funciona
         O resultado desse código é todos os marcadores são exibidos conforme o esperado. 
         Mas quando se alterava a fonte de dados e chamava novamente a função dnovamente, o dados não era como esperado. 
            Link utlizado: https://github.com/angular-ui/ui-leaflet/issues/239       
        */

        $scope.$watchCollection("ctrl.results", function (newValue, oldValue) {
            leafletData.getDirectiveControls().then(function (controls) {
                controls.markers.create({}, ctrl.markersMap);
                var markers = ctrl.convertToMarkers(newValue);
                controls.markers.create(markers, ctrl.markersMap);
                ctrl.markersMap = markers;
            });
        });
        //Converter os dados no padrão de leitura do Leaflet 
        ctrl.convertToMarkers = function (points) {
            return points.reduce(function (total, p) {
                
                if (p.position.type === 'Point') {
                    total['a' + p.id] = {
                        id: p.id,
                        code: p.code,
                        lat: p.position.lat,
                        lng: p.position.lng,
                    };
                } else if (p.position.type === 'MultiPoint') {
                    for (var i = 0; i < p.position.points.length; i++) {
                        var pp = p.position.points[i];
                        total['a' + p.id + 'i' + i] = {
                            id: p.id,
                            code: p.code || '',
                            lat: pp.lat,
                            lng: pp.lng,
                        };

                    }

                }

                
                return total;
            }, {});

        };
        ctrl.map = {
            center: {
                lat: -22.3169854,
                lng: -43.41331029,
                zoom: 9
            }
        };
    }]
    }
});