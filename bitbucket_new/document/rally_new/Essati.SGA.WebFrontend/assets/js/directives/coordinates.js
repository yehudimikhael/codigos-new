﻿angular.module('SGA').directive('coordinates', function () {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            sgaCoordinates: '=',
            sgaMarkers: '=',
            sgaAdd: '&',
            sgaDelete: '&',
            sgaChange: '&',
            sgaInputSrid: "="
        },
        replace: true,
        templateUrl: "assets/js/directives/templates/coordinates.html",
        controllerAs: 'ctrl',
        controller: ['$scope', function ($scope) {
            var ctrl = this;
            ctrl.currentDatum = 0;
            ctrl.currentType = 0;
            ctrl.test = 0;
            ctrl.handleSridChange = function (newSrid, oldSrid) {
                var sridIndexes = ctrl.lookupSRIDToIndex(newSrid);
                var sridIndexesOld = ctrl.lookupSRIDToIndex(oldSrid);
                if (sridIndexes.found) {
                    ctrl.currentDatum = sridIndexes.datum;
                    ctrl.currentType = sridIndexes.type;
                } else {
                    console.log("Não foi possível determinar os indices do SRID!");
                    //Setando para geográfico/SIRGAS
                    ctrl.currentDatum = 0;
                    ctrl.currentType = 0;
                }

                //No caso de alteração de coordenadas projetadas para geográficos ou vice versa, altera o "Object Type Descriptor" de cada coordenada
                //O API precisa disso para a deserialização
                var objType = null;
                if (sridIndexesOld.type === 0 && sridIndexes.type !== 0) {
                    console.log("Alterando OTD das coordenadas para XY");
                    // coord.$type = "Essati.SGA.Models.GeneralComplexTypes.PointXY, Essati.SGA.Models";
                    objType = "Essati.SGA.Models.GeneralComplexTypes.PointXY, Essati.SGA.Models";
                } else if (sridIndexesOld.type !== 0 && sridIndexes.type === 0) {
                    console.log("Alterando OTD das coordenadas para LatLong");
                    objType = "Essati.SGA.Models.GeneralComplexTypes.PointLatLng, Essati.SGA.Models";
                }
                

                if (!!oldSrid) {
                    console.log("Reprojetando as coordenadas");
                    for (var i = 0; i < ctrl.sgaCoordinates.length; i++) {
                        var coord = ctrl.sgaCoordinates[i];

                        //Seta o tipo (pode ter mudado ou não
                        coord.$type = objType || coord.$type;

                        //Reprojeta a coordenada
                        var projected = proj4("EPSG:" + oldSrid, "EPSG:" + newSrid, [coord.lng, coord.lat]);

                        //Aredonda para no máximo 6 decimais, para limpar artefatos de calculo de floating point. 6 decimais permite um precição de aprox 10cm.
                        //o + serve para tirar os zeros finais
                        coord.lng = +(Math.round(projected[0] + "e+6") + "e-6");
                        coord.lat = +(Math.round(projected[1] + "e+6") + "e-6");
                    }
                }
            };

            ctrl.updateCoordinate = function (coord, index) {
                var marker = this.sgaMarkers[index];
                if (!isNaN(coord.lat) && coord.lat !== null && !isNaN(coord.lng) && coord.lng !== null) {
                   
                    //Verificar se o SRID atual é 4326, se não for, projeta primeiro
                    //Atenção: Apesar do fato que se fala de Lat/Long, a ordem "correta" (discussão eterna), é Long/Lat. É assim que o proj4 espera tb
                    if (ctrl.sgaInputSrid === 4326) {
                        marker.lng = coord.lng;
                        marker.lat = coord.lat;
                    } else {
                        var projected = proj4("EPSG:" + ctrl.sgaInputSrid, "WGS84", [coord.lng, coord.lat]);
                        marker.lng = projected[0];
                        marker.lat = projected[1];
                        
                    }
                 

                  

                    this.sgaChange();
                }
            }

            ctrl.deleteCoordinate = function (index) {

                var marker = this.sgaMarkers.splice(index, 1);
                var coord = this.sgaCoordinates.splice(index, 1);

                this.sgaDelete(coord, marker);
                this.sgaChange();
            };

            ctrl.addCoordinate = function () {
                this.sgaMarkers.push({});
                this.sgaCoordinates.push({ $type: "Essati.SGA.Models.GeneralComplexTypes.PointLatLng, Essati.SGA.Models" }); //Necessario para a deserialização no lado do servidor
                this.sgaAdd();
                this.sgaChange();

            };

            ctrl.lookupIndexToSRID = function (datumIndex, typeIndex) {
                return ctrl.sridLookup[datumIndex][typeIndex];
            }

            ctrl.lookupSRIDToIndex = function (srid) {
                var datumIndex = 0;
                var typeIndex = 0;
                var currentDatum = []; // Lista de tipos 
                for (datumIndex = 0; datumIndex < ctrl.sridLookup.length; datumIndex++) {
                    currentDatum = ctrl.sridLookup[datumIndex];
                    for (typeIndex = 0; typeIndex < currentDatum.length; typeIndex++) {
                        if (currentDatum[typeIndex] === srid) {
                            return { found: true, datum: datumIndex, type: typeIndex };
                        }
                    }

                }
                return { found: false };
            }

            


            //Tabela de determinação do codidos do EPSG para o SRID a usar
            ctrl.sridLookup = [
                [
                    4674, //	GCS SIRGAS 2000 (geográfico)
                    31978, //	SIRGAS 2000 / UTM zone 18S
                    31979, //	SIRGAS 2000 / UTM zone 19S
                    31980, //	SIRGAS 2000 / UTM zone 20S
                    31981, //	SIRGAS 2000 / UTM zone 21S
                    31982, //	SIRGAS 2000 / UTM zone 22S
                    31983, //	SIRGAS 2000 / UTM zone 23S
                    31984, //	SIRGAS 2000 / UTM zone 24S
                    31985 //	SIRGAS 2000 / UTM zone 25S
                ],
                [
                    4326, //	GCS WGS84 (geográfico)
                    32718, //	WGS 84 / UTM zone 18S
                    32719, //	WGS 84 / UTM zone 19S
                    32720, //	WGS 84 / UTM zone 20S
                    32721, //	WGS 84 / UTM zone 21S
                    32722, //	WGS 84 / UTM zone 22S
                    32723, //	WGS 84 / UTM zone 23S
                    32724, //	WGS 84 / UTM zone 24S
                    32725 //	WGS 84 / UTM zone 25S
                ],
                [
//SAD69 não pode ser mais usado para levantamentos novos, mas incluimos para ter compatibilidade com dados levantados no passado
                    4618, //	GCS SAD69 (geográfico)
                    29188, //	SAD69 / UTM zone 18S
                    29189, //	SAD69 / UTM zone 19S
                    29190, //	SAD69 / UTM zone 20S
                    29191, //	SAD69 / UTM zone 21S
                    29192, //	SAD69 / UTM zone 22S
                    29193, //	SAD69 / UTM zone 23S
                    29194, //	SAD69 / UTM zone 24S
                    29195 //	SAD69 / UTM zone 25S
                ]
            ];

            ctrl.coordinateTypes = [{ id: 0, name: "Geográfico" },
                                    { id: 1, name: "UTM 18S" },
                                    { id: 2, name: "UTM 19S" },
                                    { id: 3, name: "UTM 20S" },
                                    { id: 4, name: "UTM 21S" },
                                    { id: 5, name: "UTM 22S" },
                                    { id: 6, name: "UTM 23S" },
                                    { id: 7, name: "UTM 24S" },
                                    { id: 8, name: "UTM 25S" },
                                   ];
        

            //var srid = 0;
            //srid = ctrl.lookupSRIDToIndex(ctrl.sgaInputSrid);
            //if (srid.found) {
            //    ctrl.currentDatum = srid.datum;
            //    ctrl.currentType = srid.type;
            //} else {
            //    console.log("Não foi possível determinar o SRID!");

            //    //Setando para geográfico/SIRGAS
            //    ctrl.currentDatum = 0;
            //    ctrl.currentType = 0;
            //}


            $scope.$watch(
                  function watchSrid(scope) {
                      // Return the "result" of the watch expression.
                      return (ctrl.sgaInputSrid);
                  },
                  function handleChange(newValue, oldValue) {
                      ctrl.handleSridChange(newValue, oldValue);
                  }
              );
            
            $scope.$watch(
                function watchSrid(scope) {
                    // Return the "result" of the watch expression.
                    return (ctrl.currentDatum);
                },
                function handleChange(newValue, oldValue) {
                    ctrl.sgaInputSrid = ctrl.lookupIndexToSRID(ctrl.currentDatum, ctrl.currentType);
                }
            );

            $scope.$watch(
               function watchSrid(scope) {
                   // Return the "result" of the watch expression.
                   return (ctrl.currentType);
               },
               function handleChange(newValue, oldValue) {
                   ctrl.sgaInputSrid = ctrl.lookupIndexToSRID(ctrl.currentDatum, ctrl.currentType);
               }
           );

        }
    ]};
});

