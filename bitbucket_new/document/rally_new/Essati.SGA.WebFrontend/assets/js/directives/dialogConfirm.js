﻿angular.module('SGA').directive('dialogConfirm', ['prompt', '$rootScope', '$location', '$state', 'dialogService' ,function (prompt, $rootScope, $location, $state, dialogService) {

    //Modelo da chamada do directive
    //<dialog-confirm title-temp="Salvar" text-temp="Você deseja salvar ?" delete-temp="true"></dialog-confirm>

    return {
        restrict: 'E',
        bindToController: {
            titleTemp: '@titleTemp',
            textTemp: '@textTemp',
            deleteTemp: '@deleteTemp',
            sgaSave: '&',
            sgaDelete: '&'

        },
        scope: {},
        templateUrl: "assets/templates/Dialog/dialogConfirm.html",
        controllerAs: 'ctrl',
        controller: function () {
            var ctrl = this;
            ctrl.delete = ctrl.deleteTemp; //Check para liberar o botão de Apagar
            ctrl.backPage = backPage; //Função para o botão voltar Pagina
            ctrl.showConfirm = showConfirm; //Função para Mostrar o Dialog Confirm
            ctrl.showDelete = showDelete; //Função para Layout de Deleter Arquivo
            
            //Criado um Objeto que armazena as configurações que vai aparecer no Layout
            ctrl.options = {
                title: ctrl.titleTemp,
                message: ctrl.textTemp,
                input: false,
                buttons: [
                { label: 'Cancelar', cancel: true },
                { label: 'OK', primary: true }
                ]
            };

            function showDelete() {
                ctrl.options.title = 'Deletar',
                ctrl.options.message = 'Você deseja realmente apagar ?'
                var option = angular.copy(ctrl.options);
                prompt(option).then(
                    function () {
                    ctrl.sgaDelete(); //Chama a função delete no Controller
                }, function () {                   
                    //Quando o usuário cancela 
                })
            }
            function showConfirm() {          
                var option = angular.copy(ctrl.options);
                prompt(option).then(function () {
                    ctrl.sgaSave(); //Chama a função Salvar no Controller
                }, function () {                    
                    //Quando o usuário cancela 
                })
            }
           //Função que chama o state anterior visitado
            function backPage() {               
                $location.path(dialogService.previous); //Chama o state anterior declarado no app.js
            }
        }
    }
}])