﻿angular.module('SGA').directive('photoList',['Lightbox', function (Lightbox) {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            photos: '=sgaPhotos',
            change: '&sgaChange',
            Add: '=sgaAdd', //feito para removar o botão de ADD quando só quiser mostrar as fotos,
            showControls: '=sgaShowControls'
        },
        replace: true,
        templateUrl: "assets/js/directives/templates/photos.html",
        controllerAs: 'ctrl',
        controller: ['Upload','settings', function (Upload, settings) {
            var ctrl = this;
            this.fileQueue = [];
            ctrl.viewAdd = ctrl.Add; //feito para removar o botão de ADD foto quando só quiser mostrar as fotos. tem um ng-show 
            // upload on file select or drop
            this.upload = function (file) {
                Upload.upload({
                    url: settings.sgaApiBaseUrl + 'temp/photo',
                    data: { file: file }
                }).then(function (resp) {
                    //console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                    ctrl.photos.push.apply(ctrl.photos, resp.data);
                    ctrl.fileQueue = [];
                    ctrl.change();
                }, function (resp) {
                    console.log('Error status: ' + resp.status);
                }, function (evt) {
                    var progressPercentage = Math.min(parseInt(100.0 * evt.loaded / evt.total));
                    file.progress = progressPercentage;
                    // console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                });
            };

            // for multiple files:
            this.uploadFiles = function (files) {
                if (files && files.length) {
                    for (var i = 0; i < files.length; i++) {
                        ctrl.upload(files[i]);
                    }
                }
            };

            this.deletePhoto = function (photoIndex) {
                var photo = ctrl.photos[photoIndex];
                ctrl.shiftPhoto(photoIndex, ctrl.photos.length - photoIndex - 1);
                photo.state = 'Deleted';
                ctrl.change();
            };

            this.shiftPhoto = function (photoIndex, numPositions) {

                //Once deleted, don't modify the state
                if (ctrl.photos[photoIndex].state !== 'Deleted') {
                    ctrl.photos[photoIndex].state = 'Modified';
                }

                if (numPositions === -1) { //normal backshifting
                    if (ctrl.photos[photoIndex + numPositions].state !== 'Deleted') {
                        ctrl.photos[photoIndex + numPositions].state = 'Modified';
                    }
                } else { //normal forward shifting, plus delete jumps
                    for (var i = 0; i <= numPositions; i++) {
                        if (ctrl.photos[photoIndex + i].state !== 'Deleted') {
                            ctrl.photos[photoIndex + i].state = 'Modified';
                        }
                    }
                }
                
                //swap the position of the photo
                ctrl.photos.splice(photoIndex + numPositions, 0, ctrl.photos.splice(photoIndex, 1)[0]);

                //Fire the change event
                ctrl.change();
            };

            this.markChanged = function (photoIndex) {
                if (ctrl.photos[photoIndex].state !== 'Deleted' && ctrl.photos[photoIndex].state !== 'Added') {
                    ctrl.photos[photoIndex].state = 'Modified';
                }
            }
            //Função para organizar as dados de forma que o Lightbox consiga ler
            this.openLightboxModal = function (index) {
                
                var arrayPhoto = [];
                for (var i = 0; i < ctrl.photos.length; i++) {
                    arrayPhoto.push({
                        description: ctrl.photos[i].description,
                        url: ctrl.photos[i].filePath,
                        thumbnailPath: ctrl.photos[i].thumbnailPath
                    })
                }
                Lightbox.openModal(arrayPhoto, index);
           
            };

        }

    ]};
}]);