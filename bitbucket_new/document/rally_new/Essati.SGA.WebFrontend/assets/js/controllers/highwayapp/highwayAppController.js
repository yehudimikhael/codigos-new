﻿angular.module('SGA').controller('HighwayAppController', [
    '$http',
    '$sce',
    'searchService',
    'settings',
    '$scope',
    '$stateParams',
    'appService',
    function ($http,
    $sce,
    searchService,
    settings,
    $scope,
    $stateParams,
    appService) {
        var ctrl = this;
        ctrl.view = 0;
        ctrl.markers = [];
        ctrl.accessRoad = appService.get.query(null, function () {            
            var i = 0;
            while (i < ctrl.accessRoad.length) {
                if (ctrl.markers.length == ctrl.accessRoad.length) {
                    break;
                }
                ctrl.markers.push({
                    idaccessRoad: ctrl.accessRoad[i].idaccessRoad,
                    lat: ctrl.accessRoad[i].position.points[0].lat,
                    lng: ctrl.accessRoad[i].position.points[0].lng,
                })

                i++;
            }
        });
    }]);