﻿angular.module('SGA').controller('MapController', [
    '$http',
    '$sce',
    'searchService',
    'settings',
    '$scope',
    '$stateParams',
    'appService',
    'mapService',
    'leafletMarkerEvents',
    function (
        $http,
        $sce,
        searchService,
        settings,
        $scope,
        $stateParams,
        appService,
        mapService,
        leafletMarkerEvents) {
        var ctrl = this;
        ctrl.search = searchService.Instance($stateParams.entityName);
        //Fica monitorando o Mapa e quando algum marker é clicado faz um get pelo id para buscar as informações referênte ao marker clicado
        $scope.$on('leafletDirectiveMarker.main.click', function (e, args) {
            ctrl.marker = mapService.mapArea.get({ entityName: $stateParams.entityName , id: args.model.id || 0 });
        })
    }
])