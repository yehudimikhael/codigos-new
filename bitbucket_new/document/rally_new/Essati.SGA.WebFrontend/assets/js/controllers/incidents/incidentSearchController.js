﻿angular.module('SGA').controller('IncidentSearchController', [
                 '$timeout'
                 , '$q'
                 , 'searchService'
                 , 'settings'
                 , '$scope'
                 , '$filter'
                 , '$stateParams',
        function ($timeout, $q, searchService, settings, $scope, $filter, $stateParams) {

            var ctrl = this;

            ctrl.search = searchService.Instance('incident');
        }]);