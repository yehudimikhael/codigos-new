﻿angular.module('SGA').factory('Elements', ['$resource', 'settings', function ($resource, settings) {

    return $resource(settings.sgaApiBaseUrl + 'element/:id');

}]);
