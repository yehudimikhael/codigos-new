﻿angular.module('SGA').controller('RoadSideController', [
        '$http', '$stateParams', 'searchService', 'RoadSides', function ($http, $stateParams, searchService, RoadSides) {

            this.search = searchService.Instance('roadside');
            this.roadside = RoadSides.get({ id: $stateParams.id });

        }]);



angular.module('SGA').factory('RoadSides', ['$resource', 'settings', function ($resource, settings) {

    return $resource(settings.sgaApiBaseUrl + 'roadside/:id');

}]);