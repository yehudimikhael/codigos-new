﻿angular.module('SGA').controller('DirectionController', [
        '$http', '$stateParams', 'searchService', 'Directions', function ($http, $stateParams, searchService, Directions) {

            this.search = searchService.Instance('direction');
            this.direction = Directions.get({ id: $stateParams.id });

        }]);

angular.module('SGA').factory('Directions', ['$resource', 'settings', function ($resource, settings) {

    return $resource(settings.sgaApiBaseUrl + 'direction/:id');

}]);