﻿(function () {
    "use strict";

    angular
        .module("SGA")
        .controller("LoginController",
                    ["userAccount",
                        "currentUser",
                        "$state",
                        loginController]);

    function loginController(userAccount, currentUser, $state) {
        var vm = this;

        //vm.checkLoggin = function () {
        //    if (!currentUser.getProfile().isLoggedIn) {
        //        $state.go('login');
        //    } else {
        //        return currentUser.getProfile().isLoggedIn;
        //    }
        //}


        vm.isLoggedIn = function () {
            return currentUser.getProfile().isLoggedIn;
        };
        vm.logout = function () {
            currentUser.clearProfile();
            $state.go('login', {}, { reload: true });

        }

        vm.currentUser = currentUser.getProfile().username;
        vm.login = function () {
            vm.userData.grant_type = "password";
            vm.userData.userName = vm.userData.email;
            userAccount.login.loginUser(vm.userData,
                function (data) {
                    vm.message = "";
                    vm.password = "";
                    currentUser.setProfile(vm.userData.userName, data.access_token);
                    $state.go('home', {}, { reload: true });
                },
                function (response) {
                    vm.password = "";
                    if (response.data.error === "invalid_grant") {
                        //vm.message = treatmentService.log;
                        //vm.message = "Usuário e/ou senha incorreto!";
                        
                    }
                });

        }
    }
})();