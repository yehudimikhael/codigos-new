﻿angular.module('SGA').controller('RoadsideOccupationSearchController', [
                 '$timeout'
                 , '$q'
                 , 'searchService'
                 , 'settings'
                 , '$scope'
                 , '$filter'
                 , '$state'
                 , '$stateParams'
                 ,

        function ($timeout, $q, searchService, settings, $scope, $filter, $state, $stateParams) {

            var ctrl = this;

            ctrl.search = searchService.Instance('roadsideoccupation');
        }]);