﻿angular.module('SGA').controller('AccountController', [
                    '$timeout'
                    , '$q'
                    , '$state'
                    , '$http'
                    , '$resource'
                    , 'searchService'
                    , 'settings'
                    , 'dialogService'
                    , 'messageService'
                    , '$scope'
                    , '$filter'
                    , '$stateParams'
                    , 'accountService'
                    , 'currentUser',
                    function (
                        $timeout,
                        $q,
                        $state,
                        $http,
                        $resource,
                        searchService,
                        settings,
                        dialogService,
                        messageService,
                        $scope,
                        $filter,
                        $stateParams,
                        accountService,
                        currentUser) {
                        var ctrl = this;
                        this.listUser = [];
                        this.nomepapel = null;
                        this.checkemail = null;
                        this.checkpass = null;

                        //listar os Person

                        //Verifica se o usuário já existe
                        ctrl.check = function () {

                            var val = $http.get(settings.sgaApiBaseUrl + 'account/validation/' + ctrl.getuser.email).success(function (data) {
                                ctrl.checkuser = data;
                            });
                        }

                        //Listar Role do Usuário Substituir userRoler para accountUser

                        ctrl.user = accountService.accountUser.query({ email: $stateParams.email || 0 });

                        //Listar os  Papeis
                        ctrl.listroles = accountService.listRole.query();

                        ctrl.listcontract = function (query) {
                            return accountService.contract.query().$promise;
                        };

                        //Listar a Organização
                        ctrl.organization = accountService.organizationList.query();
                        
                        //Pegar o template do Modelo Usuário para Salvar
                        ctrl.getuser = accountService.getAccountModel.query();
                        

                        //this.save = function (value) { return accountService.saveUser.save(value); }
                        ctrl.save = function () {
                            accountService.saveUser.save(this.getuser,
                             function () {
                                 var title = 'Salvo';
                                 var message = 'Usuário salvo com sucesso';
                                 messageService.send(message, title);
                                 $state.reload();
                             },
                            function () {
                                var title = 'Problema';
                                var message = 'Problema ao salvar, contate o administrador';
                                messageService.send(message, title);
                            });

                        }
                        //accountService.save.put();

                        //Deleta Usuario
                        ctrl.delete = function (value) {
                            accountService.deleteAccount.delete({ id: value },
                                 function () {
                                     var title = 'Apagado';
                                     var message = 'Usuário apagado com sucesso';
                                     messageService.send(message, title);
                                     $state.go('account', {}, { reload: true });
                                     
                                     
                                 },
                                function () {
                                    var title = 'Problema';
                                    var message = 'Problema ao apagar, contate o administrador';
                                    messageService.send(message, title);
                                    
                                });
                        }

                        //Enviar os dados para serem salvos

                        ctrl.addRole = function () {
                            ctrl.user.userRoles = ctrl.user.userRoles || [];
                            var i = ctrl.user.userRoles;
                            i.push({
                                idrole: "",
                                contractList: "",
                                state: 'Added',
                                isAdmin: "",
                            })

                        }
                        ctrl.role = null;

                        ctrl.changerole = function (role) {
                            role.state = 'Modified';
                        }
                        ctrl.deleterole = function (role) {
                            role.state = 'Deleted';
                        }
                        ctrl.saveAlter = function () {

                            accountService.saveUser.save(
                                this.user,
                                function () {
                                    var title = 'Salvo';
                                    var message = 'Usuário salvo com sucesso';
                                    messageService.send(message, title);
                                },
                            function () {
                                var title = 'Problema';
                                var message = 'Problema ao salvar, contate o administrador';
                                messageService.send(message, title);
                            });
                        }
                        ctrl.roledel = function () {
                            accountService.saveUser.save(ctrl.role,
                                function () {
                                    var title = 'Salvo';
                                    var message = 'Usuário salvo com sucesso';
                                    messageService.send(message, title);
                                },
                            function () {
                                var title = 'Problema';
                                var message = 'Problema ao salvar, contate o administrador';
                                messageService.send(message, title);
                            });
                        }
                        this.add = function () {
                            var i = ctrl.getuser.userRoles || [];
                            if (i.length === 0) {
                                i.push({
                                    idrole: "",
                                    contractList: "",
                                    state: 'Added',
                                    isAdmin: "",
                                });
                            } else {
                                i.push({
                                    idrole: "",
                                    contractList: "",
                                    state: 'Added',
                                    isAdmin: "",
                                });
                            }
                        }




                    }]);
