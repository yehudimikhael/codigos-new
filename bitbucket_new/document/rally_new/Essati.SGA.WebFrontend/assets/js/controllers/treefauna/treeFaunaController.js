﻿angular.module('SGA').controller('treeFaunaController', [
    '$http',
    '$resource',
    'searchService',
    'settings',
    '$stateParams',
    'loadingService',
    function ($http, $resource, searchService, settings, stateParams, loadingService) {

        var ctrl = this;
        ctrl.id = null;

        ctrl.my_tree_handler = function (branch) {
            var _ref;
            ctrl.output = "You selected: " + branch.label;
            if ((_ref = branch.data) !== null ? _ref.description : void 0) {
                return ctrl.output += '(' + branch.data.description + ')';
            }
        }
        ctrl.apple_selected = function (branch) {
            return ctrl.output = "APPLE! : " + branch.label;
        };
        ctrl.my_tree = {};

        //  ctrl.nodes = [
        //{
        //    label: 'Animal',
        //    children: [
        //      {
        //          label: 'Dog',
        //          data: {
        //              description: "man's best friend"
        //          }
        //      }, {
        //          label: 'Cat',
        //          data: {
        //              description: "Felis catus"
        //          }
        //      }, {
        //          label: 'Hippopotamus',
        //          data: {
        //              description: "hungry, hungry"
        //          }
        //      }, {
        //          label: 'Chicken',
        //          children: ['White Leghorn', 'Rhode Island Red', 'Jersey Giant']
        //      }
        //    ]
        //},
        //{
        //    label: 'Vegetable',
        //    data: {
        //        definition: "A plant or part of a plant used as food, typically as accompaniment to meat or fish, such as a cabbage, potato, carrot, or bean.",
        //        data_can_contain_anything: true
        //    },
        //    onSelect: function (branch) {
        //        return ctrl.output = "Vegetable: " + branch.data.definition;
        //    },
        //    children: [
        //      {
        //          label: 'Oranges'
        //      }, {
        //          label: 'Apples',
        //          children: [
        //            {
        //                label: 'Granny Smith',
        //                onSelect: ctrl.apple_selected,
        //                children: [
        //            {
        //                label: 'Granny Smith com nome muuuuito maiiiior',
        //                onSelect: ctrl.apple_selected
        //            }, {
        //                label: 'Red Delicous',
        //                onSelect: ctrl.apple_selected
        //            }, {
        //                label: 'Fuji',
        //                onSelect: ctrl.apple_selected
        //            }
        //                ]
        //            }, {
        //                label: 'Red Delicous',
        //                onSelect: ctrl.apple_selected
        //            }, {
        //                label: 'Fuji',
        //                onSelect: ctrl.apple_selected
        //            }
        //          ]
        //      }
        //    ]
        //},

        //  ]
        ctrl.nodes = [];
       
        //Pegas as informações no backend
        $http.get("http://localhost:10000/api/TerritorialUnit").then(
            function (n) {
                ctrl.nodes = n.data;               
            });
    }
]);
