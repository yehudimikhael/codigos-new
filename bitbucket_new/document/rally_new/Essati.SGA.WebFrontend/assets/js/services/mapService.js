﻿angular.module('SGA').factory('mapService', ['$resource', 'settings', mapService])
function mapService($resource, settings) {
    return {
        mapArea: $resource(settings.sgaApiBaseUrl + '/:entityName', null, { 
            'get': { method: 'GET', params: { id: '@id', entityName: '@entityName' }, isArray: false, url: settings.sgaApiBaseUrl + '/:entityName/:id/reduced' },
        })
    }
}