﻿angular.module('SGA').factory('protectedareaService', ['$resource', 'settings', protectedareaService])
function protectedareaService($resource, settings) {
    return {
        protectedArea: $resource(settings.sgaApiBaseUrl + '/protectedarea', null, {
            'post': { method: 'POST', params: { id: '@id' }, isArray: true, url: settings.sgaApiBaseUrl + '/protectedareas' },
            'search':{method: 'GET', isArray: true, url: settings.sgaApiBaseUrl + '/protectedareas/searchparams'},
            'getId': { method: 'GET', params: { id: '@id' }, isArray: false, url: settings.sgaApiBaseUrl + '/protectedarea/:id/reduced' },
            'getLevelOfErosion': { method: 'GET', isArray: true, url: settings.sgaApiBaseUrl + 'protectedarea/leveloferosion' },
            'getNaturalRegenerationState': { method: 'GET', isArray: true, url: settings.sgaApiBaseUrl + 'protectedarea/naturalregenerationstate' },
            'getProtectedCondition': { method: 'GET', isArray: true, url: settings.sgaApiBaseUrl + 'protectedarea/protectedcondition' },
            'getProtectedAreaType': { method: 'GET', isArray: true, url: settings.sgaApiBaseUrl + 'protectedarea/protectedareatype' },
            'getReliefType': { method: 'GET', isArray: true, url: settings.sgaApiBaseUrl + 'protectedarea/relieftype' },
            'getSeasonaLity': { method: 'GET', isArray: true, url: settings.sgaApiBaseUrl + 'protectedarea/seasonality' },
            'getTreeDensity': { method: 'GET', isArray: true, url: settings.sgaApiBaseUrl + 'protectedarea/treedensity' },
            'getUndergrowthDensity': { method: 'GET', isArray: true, url: settings.sgaApiBaseUrl + 'protectedarea/undergrowthdensity' },
            'getVegetationPosition': { method: 'GET', isArray: true, url: settings.sgaApiBaseUrl + 'protectedarea/vegetationposition' },
            
        })
    }
}