﻿/*
*
* Factory: SearchService
* Author: Jos Bol
* Function: Creates instances of a search service based on the entity name
* Uses: Angular standard services, Settings
*/

angular.module('SGA').factory('searchService', [
       '$http', '$timeout', '$location', 'settings', function ($http, $timeout, $location, settings,$state) {

           /*Constructor, accepts a string with an entityname (ex. "Incident") */
           function Instance(entityName) {

               this.url = settings.sgaApiBaseUrl + entityName + 's';
               this.entityName =  entityName;
               this.results = [];
               this.currentSearchType = null;
               this.currentArgument = "";
               this.searchTypes = [];

           }
           
           /* Gets the possible search parameters from the webservice */
           //TODO: move this into the constructor to make it private. Should onyl be called once anyway
           Instance.prototype.getParams = function () {
               var self = this;
               console.log("updating params");
               $http.get(this.url + '/searchparams'
               ).then(function (response) {
                   self.searchTypes = response.data;
                   return response;
               });
           }

           /* Processes a request to update the results list. OBS: Anti-hammering, using a configurable delay found in the settings service */
           Instance.prototype.updateList = function () {
               console.log("update list");
               var self = this;
               var par = { Type: this.currentSearchType, Argument: this.currentArgument };

               if (this.searchPending) {
                   $timeout.cancel(this.searchPending);
               }

               //To avoid hammering the webservice, we only really make the request after updateList hasn't been called for the time set in "autoCompleteDelay"
               this.searchPending = $timeout(
                   function () {
                       console.log("getting list");

                       $http.post(self.url, par)
                           .then(function (response) {
                               self.results = response.data;
                           });
                   }, settings.autoCompleteDelay); //Configurable delay used here


           };

           //Set the current searchType
           Instance.prototype.setSearchType = function (type) {
               var self = this;
               //If the old and new searchtypes are incompatible, empty the search expression
               if (self.currentSearchType && self.currentSearchType.dataType !== type.dataType) {
                   self.currentArgument = "";
               }

               //Set the new search expression
               self.currentSearchType = type;

               //Update the list
               self.updateList();

           }

           Instance.prototype.redirectToItem = function (item) {
               $location.path($state.current.data.module + '/' + this.entityName + 's/' + item.id);
               $location.replace();
           }

           //Return the new instance, already
           return {
               Instance: function (entityName) {
                   var i = new Instance(entityName);
                   i.getParams();
                   return i;
               }
           };

       }
]);