﻿angular.module('SGA').factory('accountService', ['$resource', 'settings', accountService]);

function accountService($resource, settings) {

    return {
        check: $resource(settings.sgaApiBaseUrl + 'account/validation/:email', null, {
            'query': { method: 'GET', params: { email: '@email' }, isArray: false }
        }),
        params: $resource(settings.sgaApiBaseUrl + 'accounts/searchparams', null, {
            'query': { method: 'GET', isArray: true }
        }),
        person: $resource(settings.sgaApiBaseUrl + 'account/listuser', null, {
            'query': { method: 'GET', isArray: true }
        }),
        organizationList: $resource(settings.sgaApiBaseUrl + 'account/organizationlist', null, {
            'query': { method: 'GET', isArray: true }
        }),
        singlePerson: $resource(settings.sgaApiBaseUrl + 'account/person/:id', null, {
            'put': { method: 'PUT', params: { id: '@id' } }
        }),
        listRole: $resource(settings.sgaApiBaseUrl + 'account/rolelist', null, {
            'query': { method: 'GET', isArray: true }
        }),
        contract: $resource(settings.sgaApiBaseUrl + 'account/listprefix', null, {
            'query': {method: 'GET', isArray: true}
        }),
        accountUser: $resource(settings.sgaApiBaseUrl + 'account/:email', null, {
            'query': { method: 'GET', params:{email:'@email'}, isArray: false }
        }),
        getAccountModel: $resource(settings.sgaApiBaseUrl + 'account/get', null, {
            'query': { method: 'GET' }
        }),
        saveUser: $resource(settings.sgaApiBaseUrl+ 'account/add', null, {
            'save':{method:'POST'}
        }),
        deleteAccount: $resource(settings.sgaApiBaseUrl + 'account/:id', null, {
            'delete': { method: 'DELETE', params: { id: '@id' }, isArray: false }
        })

    }
}