﻿angular.module('SGA').factory('appService', ['$resource', 'settings', appService]);
function appService($resource, settings) {
    return {
        get: $resource(settings.sgaApiBaseUrl + 'app/accessroad', null, {
            'query': { method: 'GET', isArray: true },
            'getAccessRoadID': { method: 'GET', params: { id: '@id' }, isArray: true, url: settings.sgaApiBaseUrl + 'app/accessroad/:id' },
            'getdirect': { method: 'GET', isArray: true, url: settings.sgaApiBaseUrl + 'app/direction' },
        }),
    }
}