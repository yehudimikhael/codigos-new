﻿
angular
    .module('SGA')
    .factory('userAccount',
    [
        '$resource',
        'settings',
        userAccount
    ]);

function userAccount($resource, settings) {
    return {
        registration: $resource(settings.baseUrl + 'Account/Register', null,
        {
            'registerUser': { method: 'POST' }
        }),
        login: $resource(settings.sgaTokenUrl, null,
        {
            'loginUser': {
                method: 'POST',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                transformRequest: function (data, headersGetter) {
                    var str = [];
                    for (var d in data)
                        str.push(encodeURIComponent(d) + '=' +
                            encodeURIComponent(data[d]));
                    return str.join('&');
                }

            }
        }),
        test: $resource(settings.sgaApiBaseUrl + 'Account/TestRestricted', null, {
            'test': { method: 'POST' }
        }),

        userInfo: $resource(settings.baseUrl + 'Account/UserInfo', null, {
            'userInfo': { method: 'GET' }
        })
    }
}

