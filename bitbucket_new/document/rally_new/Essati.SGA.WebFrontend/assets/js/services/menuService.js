﻿angular.module('SGA').factory('menuService', ['$resource', 'settings', menuService]);

function menuService($resource, settings) {
    return {

        menu:$resource(settings.sgaApiBaseUrl + 'menu', null, {
            'query': { method: 'GET'} 
        }),
    }
}
