﻿angular
    .module('SGA')
    .service('messageService', [messageService]);

function messageService() {
    var ctrl = this;
    ctrl.messages = [];
    this.send = function (message, title) {
        ctrl.messages.push({ text: message, titl: title});
    };
    this.delete = function () {
        ctrl.messages = [];
    };
}
