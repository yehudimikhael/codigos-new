﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using Essati.SGA.WebFrontend.Models;
using Microsoft.AspNet.Identity;
using Essati.SGA.DAL.EntityClasses;
using Microsoft.Ajax.Utilities;

namespace Essati.SGA.WebFrontend.Identity
{
    public class UserStore<TUser> : IUserStore<TUser>, IUserLoginStore<TUser>, IUserClaimStore<TUser>, IUserRoleStore<TUser>,
        IUserPasswordStore<TUser>, IUserSecurityStampStore<TUser>, IUserEmailStore<TUser>, IUserLockoutStore<TUser, string>,
        IUserTwoFactorStore<TUser, string>, IUserPhoneNumberStore<TUser>
        where TUser : IdentityUser, new()
    {
        private bool _disposed;

        private UserEntity ue;

        public UserStore()
        {
            this.ue = new UserEntity();
        }

        public Task CreateAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            ue.Username = user.UserName;
            ue.AccessFailedCount = user.AccessFailedCount;
            ue.Email = user.Email;
            ue.PasswordHash = user.PasswordHash;
            // user.Claims = user.Claims;
            // ue.Iduser = null;
            ue.EmailConfirmed = user.IsEmailConfirmed;
            ue.PhoneNumberConfirmed = user.IsPhoneNumberConfirmed;
            ue.LockoutEnabled = false;// user.LockoutEnabled;
            ue.LockoutEndDateUtc = DateTime.UtcNow;
            //ue.UserlOgins = user.Logins;
            ue.PhoneNumber = user.PhoneNumber;
            //ue.Roles = user.Roles;
            ue.SecurityStamp = user.SecurityStamp;
            ue.TwoFactorEnabled = user.TwoFactorAuthEnabled;
            

            //Check if a Person exists with the current Email address, if so link it to this new account. If not, create a new Person
            PersonEntity pe = new PersonEntity();
            pe.FetchUsingUCEmail(ue.Email);
            if (pe.IsNew)
            {
                ue.Person = new PersonEntity();
                ue.Person.Name = user.Email;
                ue.Person.Email = user.Email;
            }
            else
            {
                ue.Person = pe;
            }

            ue.Save(true);

            return Task.FromResult(true);
        }

        public Task DeleteAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            ue.FetchUsingUCUsername(user.UserName);
            if (!ue.IsNew)
                ue.Delete();
            return Task.FromResult(true);
        }

        public Task<TUser> FindByIdAsync(string userId)
        {

            this.ThrowIfDisposed();
            TUser user = new TUser();
            ue.FetchUsingPK(int.Parse(userId));
            if (!ue.IsNew)
            {
                user.UserName = ue.Username;
                user.AccessFailedCount = ue.AccessFailedCount ?? 0;
                user.Email = ue.Email;
                user.PasswordHash = ue.PasswordHash;

                foreach (var claim in ue.UserClaims)
                {
                    user.Claims.Add(new IdentityUserClaim(claim.ClaimType, claim.ClaimValue));
                }
                user.Id = "" + ue.Iduser;
                user.IsEmailConfirmed = ue.EmailConfirmed ?? false;
                user.IsPhoneNumberConfirmed = ue.PhoneNumberConfirmed ?? false;
                user.LockoutEnabled = ue.LockoutEnabled ?? false;
                user.LockoutEndDate = ue.LockoutEndDateUtc.HasValue ? DateTimeOffset.Now : new DateTimeOffset(ue.LockoutEndDateUtc.Value);


                foreach (var login in ue.UserLogins)
                {
                    user.Logins.Add(new UserLoginInfo(login.LoginProvider, login.ProviderKey));
                }

                user.PhoneNumber = ue.PhoneNumber;
                foreach (var role in ue.Roles)
                {
                    user.Roles.Add(role.Code);
                }
                user.SecurityStamp = ue.SecurityStamp;
                user.TwoFactorAuthEnabled = ue.TwoFactorEnabled ?? false;

                return Task.FromResult(user);
            }
            else
            {
                return Task.FromResult((TUser)null);
            }
        }

        public Task<TUser> FindByNameAsync(string userName)
        {
            this.ThrowIfDisposed();
            TUser user = new TUser();
            ue.FetchUsingUCUsername(userName);
            if (!ue.IsNew)
            {
                user.UserName = ue.Username;
                user.AccessFailedCount = ue.AccessFailedCount ?? 0;
                user.Email = ue.Email;
                user.PasswordHash = ue.PasswordHash;

                foreach (var claim in ue.UserClaims)
                {
                    user.Claims.Add(new IdentityUserClaim(claim.ClaimType, claim.ClaimValue));
                }
                user.Id = "" + ue.Iduser;
                user.IsEmailConfirmed = ue.EmailConfirmed ?? false;
                user.IsPhoneNumberConfirmed = ue.PhoneNumberConfirmed ?? false;
                user.LockoutEnabled = ue.LockoutEnabled ?? false;
                user.LockoutEndDate = ue.LockoutEndDateUtc.HasValue ? DateTimeOffset.Now : new DateTimeOffset(ue.LockoutEndDateUtc.Value);


                foreach (var login in ue.UserLogins)
                {
                    user.Logins.Add(new UserLoginInfo(login.LoginProvider, login.ProviderKey));
                }

                user.PhoneNumber = ue.PhoneNumber;
                foreach (var role in ue.UserRoles)
                {
                    user.Roles.Add(role.Role.Name);
                    ExtRole extRole = new ExtRole();
                    extRole.Name = role.Role.Code;
                    extRole.AllContracts = role.IsAdmin;
                    role.ContractModules.ForEach(cm => extRole.ContractIds.Add(cm.Idcontract));
                    user.ExtRoles.Add(extRole);

                }
                user.SecurityStamp = ue.SecurityStamp;
                user.TwoFactorAuthEnabled = ue.TwoFactorEnabled ?? false;

                return Task.FromResult(user);
            }
            else
            {
                return Task.FromResult((TUser)null);
            }
        }

        public Task UpdateAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            return Task.FromResult(true);
        }

        private void ThrowIfDisposed()
        {
            if (this._disposed)
                throw new ObjectDisposedException(this.GetType().Name);
        }

        public void Dispose()
        {
       this._disposed = true;
        }

        public Task AddLoginAsync(TUser user, UserLoginInfo login)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            if (!user.Logins.Any(x => x.LoginProvider == login.LoginProvider && x.ProviderKey == login.ProviderKey))
            {
                user.Logins.Add(login);

                ue.FetchUsingPK(int.Parse(user.Id));

                UserLoginEntity ule = ue.UserLogins.AddNew();
                ule.LoginProvider = login.LoginProvider;
                ule.ProviderKey = login.ProviderKey;
                ule.Save();
                ue.Save(true);
            }

            return Task.FromResult(true);
        }

        public Task<TUser> FindAsync(UserLoginInfo login)
        {
            throw new NotImplementedException("Ainda não implementado");

            //string loginId = Util.GetLoginId(login);

            //var loginDoc = session.Include<IdentityUserLogin>(x => x.UserId)
            //    .Load(loginId);

            //TUser user = null;

            //if (loginDoc != null)
            //    user = this.session.Load<TUser>(loginDoc.UserId);

            //return Task.FromResult(user);
        }

        public Task<IList<UserLoginInfo>> GetLoginsAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            return Task.FromResult(user.Logins.ToIList());
        }

        public Task RemoveLoginAsync(TUser user, UserLoginInfo login)
        {
            throw new NotImplementedException("Ainda não implementado");
            //this.ThrowIfDisposed();
            //if (user == null)
            //    throw new ArgumentNullException("user");

            //string loginId = Util.GetLoginId(login);
            //var loginDoc = this.session.Load<IdentityUserLogin>(loginId);
            //if (loginDoc != null)
            //    this.session.Delete(loginDoc);

            //user.Logins.RemoveAll(x => x.LoginProvider == login.LoginProvider && x.ProviderKey == login.ProviderKey);

            //return Task.FromResult(0);
        }

        public Task AddClaimAsync(TUser user, Claim claim)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            if (!user.Claims.Any(x => x.ClaimType == claim.Type && x.ClaimValue == claim.Value))
            {

                ue.FetchUsingPK(int.Parse(user.Id));
                UserClaimEntity uce = ue.UserClaims.AddNew();
                uce.ClaimType = claim.Type;
                uce.ClaimValue = claim.Value;

                uce.Save(true);
                ue.Save(true);
                user.Claims.Add(new IdentityUserClaim
                {
                    ClaimType = claim.Type,
                    ClaimValue = claim.Value
                });
            }
            return Task.FromResult(0);
        }

        public Task<IList<Claim>> GetClaimsAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");


            ue.FetchUsingPK(int.Parse(user.Id));

            IList<Claim> result = ue.UserClaims.Select(claim => new Claim(claim.ClaimType, claim.ClaimValue)).ToList();

            return Task.FromResult(result);
        }

        public Task RemoveClaimAsync(TUser user, Claim claim)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");
            var list = ue.UserClaims.Where(x => x.ClaimType == claim.Type && x.ClaimValue == claim.Value);
            ue.UserClaims.RemoveRange(list);
            user.Claims.RemoveAll(x => x.ClaimType == claim.Type && x.ClaimValue == claim.Value);
            return Task.FromResult(0);
        }

        public Task<string> GetPasswordHashAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            return Task.FromResult(user.PasswordHash);
        }

        public Task<bool> HasPasswordAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            return Task.FromResult<bool>(user.PasswordHash != null);
        }

        public Task SetPasswordHashAsync(TUser user, string passwordHash)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            user.PasswordHash = passwordHash;
            return Task.FromResult(0);
        }

        public Task<string> GetSecurityStampAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            return Task.FromResult(user.SecurityStamp);
        }

        public Task SetSecurityStampAsync(TUser user, string stamp)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            user.SecurityStamp = stamp;
            return Task.FromResult(0);
        }

        public Task AddToRoleAsync(TUser user, string role)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            if (!user.Roles.Contains(role, StringComparer.InvariantCultureIgnoreCase))
                user.Roles.Add(role);

            return Task.FromResult(0);
        }

        public Task<IList<string>> GetRolesAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            return Task.FromResult<IList<string>>(user.Roles);
        }

        public Task<bool> IsInRoleAsync(TUser user, string role)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            return Task.FromResult(user.Roles.Contains(role, StringComparer.InvariantCultureIgnoreCase));
        }

        public Task RemoveFromRoleAsync(TUser user, string role)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            user.Roles.RemoveAll(r => String.Equals(r, role, StringComparison.InvariantCultureIgnoreCase));

            return Task.FromResult(0);
        }

        public Task<TUser> FindByEmailAsync(string email)
        {
            this.ThrowIfDisposed();
            if (email == null)
                throw new ArgumentNullException("email");

            // var result = this.session.Query<TUser>().Where(u => u.Email == email).FirstOrDefault();
            TUser result = null;
            return Task.FromResult(result);
        }

        public Task<string> GetEmailAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            return Task.FromResult(user.Email);
        }

        public Task<bool> GetEmailConfirmedAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            return Task.FromResult(user.IsEmailConfirmed);
        }

        public Task SetEmailAsync(TUser user, string email)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            user.Email = email;

            return Task.FromResult(0);
        }

        public Task SetEmailConfirmedAsync(TUser user, bool confirmed)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            user.IsEmailConfirmed = confirmed;

            return Task.FromResult(0);
        }

        public Task<int> GetAccessFailedCountAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            return Task.FromResult(user.AccessFailedCount);
        }

        public Task<bool> GetLockoutEnabledAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            return Task.FromResult(user.LockoutEnabled);
        }

        public Task<DateTimeOffset> GetLockoutEndDateAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            return Task.FromResult(user.LockoutEndDate);
        }

        public Task<int> IncrementAccessFailedCountAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            user.AccessFailedCount++;

            return Task.FromResult(user.AccessFailedCount);
        }

        public Task ResetAccessFailedCountAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            user.AccessFailedCount = 0;

            return Task.FromResult(0);
        }

        public Task SetLockoutEnabledAsync(TUser user, bool enabled)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            user.LockoutEnabled = enabled;

            return Task.FromResult(0);
        }

        public Task SetLockoutEndDateAsync(TUser user, DateTimeOffset lockoutEnd)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            user.LockoutEndDate = lockoutEnd;

            return Task.FromResult(0);
        }

        public Task<bool> GetTwoFactorEnabledAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            return Task.FromResult(user.TwoFactorAuthEnabled);
        }

        public Task SetTwoFactorEnabledAsync(TUser user, bool enabled)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            user.TwoFactorAuthEnabled = enabled;

            return Task.FromResult(0);
        }

        public Task<string> GetPhoneNumberAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            return Task.FromResult(user.PhoneNumber);
        }

        public Task<bool> GetPhoneNumberConfirmedAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            return Task.FromResult(user.IsPhoneNumberConfirmed);
        }

        public Task SetPhoneNumberAsync(TUser user, string phoneNumber)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            user.PhoneNumber = phoneNumber;

            return Task.FromResult(0);
        }

        public Task SetPhoneNumberConfirmedAsync(TUser user, bool confirmed)
        {
            this.ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            user.IsPhoneNumberConfirmed = confirmed;

            return Task.FromResult(0);
        }
    }
}
