﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using WebGrease.Css.Extensions;

namespace Essati.SGA.WebFrontend.Identity
{

    public class ExtRoleCollection : List<ExtRole>
    {
        public string ToString()
        {
            string result = "";

            this.ForEach(s => result += s + ";");


            return result;
        }
       
        /// <summary>
        /// Takes a formatted ExtRole string and converts it into an ExtRole object and adds it to the collection
        /// </summary>
        /// <param name="src">Formatted string, see remarks</param>
        /// <returns>true on success, false otherwise</returns>
        /// <remarks>Format: name;{0,1};int;int;...;int; E.g. FA-READ;0;1;2;3;4; or FA-WRITE;1; </remarks>
        public bool AddFromString(string src)
        {
            ExtRole extRole = new ExtRole();
            string[] parts = src.Split(new char[] {';'}, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length < 2)
            {
                return false; //The has to be at a least a name and an indicator wheter it applies to all contracts or not
            }
            
            extRole.Name = parts[0];
            extRole.AllContracts = parts[1] == "1"; //Anything other than "1", will simply result in not having admin rights
            
            for (int i = 2; i <= parts.Length - 1; i++)
            {
                extRole.ContractIds.Add(int.Parse(parts[i]));
            }
            
            this.Add(extRole);
            return true;
        }


     }
    
    public class IdentityUser : IUser<string>
    {
        public virtual string Id { get; set; }
        public virtual string UserName { get; set; }
        public virtual string PasswordHash { get; set; }
        public virtual string SecurityStamp { get; set; }
        public virtual string Email { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual bool IsEmailConfirmed { get; set; }
        public virtual bool IsPhoneNumberConfirmed { get; set; }
        public virtual int AccessFailedCount { get; set; }
        public virtual bool LockoutEnabled { get; set; }
        public virtual DateTimeOffset LockoutEndDate { get; set; }
        public virtual bool TwoFactorAuthEnabled { get; set; }
        public virtual List<string> Roles { get; private set; }
        public virtual ExtRoleCollection ExtRoles { get; private set; }
        public virtual List<IdentityUserClaim> Claims { get; private set; }
        public virtual List<UserLoginInfo> Logins { get; private set; }

        public IdentityUser()
        {
            this.Claims = new List<IdentityUserClaim>();
            this.Roles = new List<string>();
            this.Logins = new List<UserLoginInfo>();
            this.ExtRoles = new ExtRoleCollection();

           }

        public IdentityUser(string userId, string userName)
            : this()
        {
            this.Id = userId;
            this.UserName = userName;
        }
    }

    public class ExtRole
    {
        public string Name { get; set; }
        public IList<int> ContractIds { get; set; }
        public Boolean AllContracts { get; set; }
    
        public ExtRole()
        {
            AllContracts = false;
            ContractIds = new List<int>(10);
        }

        public string ToString()
        {
            StringBuilder result = new StringBuilder();
            result.Append(Name);
            result.Append(";");
            result.Append(AllContracts ? 1 : 0);
            result.Append(";");
            ContractIds.ForEach(i=> result.Append("" + i +";" ));
            return result.ToString();
        }
    }

    public sealed class IdentityUserLogin
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string Provider { get; set; }
        public string ProviderKey { get; set; }
    }

    public class IdentityUserClaim
    {
        public virtual string ClaimType { get; set; }
        public virtual string ClaimValue { get; set; }

        public IdentityUserClaim()
        {

        }

        public IdentityUserClaim(string type, string value)
        {
            ClaimType = type;
            ClaimValue = value;
        }

    }

    public sealed class IdentityUserByUserName
    {
        public string UserId { get; set; }
        public string UserName { get; set; }

        public IdentityUserByUserName(string userId, string userName)
        {
            UserId = userId;
            UserName = userName;
        }
    }
}
