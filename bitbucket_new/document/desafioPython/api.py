from flask import Flask, jsonify, request, make_response
import jwt
import datetime 
from flask_sqlalchemy import sqlalchemy

app = Flask(__name__)
db = sqlalchemy

app.config['SECRET_KEY' = 'thisisthesecretekey']

@app.route('/unprotected')
def unprotected():
    return ''
@app.route('/protected')
def protected():
    return ''
@app.route('/login')
def login():
    auth = request.authorization
    if auth and auth.password == 'password':
        token = jwt.encode({
            'user': auth.username, 
            'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=30),
            app.
            })
        return ''
    return make_response('Could verify', 401, {'WWW-Authenticate': 'Basic realm="Login Required'})
if __name__=='__main__':
    app.run(debug=True)