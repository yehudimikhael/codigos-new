#-*-coding: utf-8 -*-
import sqlite3
import json
def getAllUser():
    conn = sqlite3.connect('usuario.db')
    c = conn.cursor()
    c.execute("SELECT * FROM usuario")
    columns = c.description
    data = [{ columns[index][0]:column for index, column in enumerate(value)} for value in c.fetchall()]
    c.close()
    return data

def getUser(id):
    conn = sqlite3.connect('usuario.db')
    c = conn.cursor()
    c.execute("""SELECT * FROM usuario where id = ?""", (id))
    #Imprimir o key:value
    columns = c.description
    data = [{ columns[index][0]:column for index, column in enumerate(value)} for value in c.fetchall()]
    c.close()
    return data