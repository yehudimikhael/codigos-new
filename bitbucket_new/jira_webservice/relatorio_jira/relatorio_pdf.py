#!/usr/ python
# coding: utf-8

from fpdf import FPDF
import sys
import PIL
import datetime
from exportN1 import total, n1, n2, atendimento
from getmonth import searchMonth
import json

title = 'Rede - Managed Services'
stitle = 'Relatório de incidentes mensal'
#subtitle = stitle.encode("latin-1","ignore")
reload(sys)
sys.setdefaultencoding('utf-8')
logo1 = '/home/ec2-user/relatorio_jira/logo1.png'
logo2 = '/home/ec2-user/relatorio_jira/logo2.png'
def month_name (number):
    if number == 1:
        mes = 'JAN'
        mes_anexo = "Janeiro"
        return (mes, mes_anexo)
    elif number == 2:
        mes = 'FEV'
        mes_anexo = "Fevereiro"
        return (mes, mes_anexo)
    elif number == 3:
        mes = 'MAR'
        mes_anexo = "Março"
        return (mes, mes_anexo)
    elif number == 4:
        mes = 'ABR'
        mes_anexo = "Abril"
        return (mes, mes_anexo)
    elif number == 5:
        mes = 'MAI'
        mes_anexo = "Maio"
        return (mes, mes_anexo)
    elif number == 6:
        mes = 'JUN'
        mes_anexo = "Junho"
        return (mes, mes_anexo)
    elif number == 7:
        mes = 'JUL'
        mes_anexo = "Julho"
        return (mes, mes_anexo)
    elif number == 8:
        mes = 'AGO'
        mes_anexo = "Agosto"
        return (mes, mes_anexo)
    elif number == 9:
        mes = 'SET'
        mes_anexo = "Setembro"
        return (mes, mes_anexo)
    elif number == 10:
        mes = 'OUT'
        mes_anexo = "Outubro"
        return (mes, mes_anexo)
    elif number == 11:
        mes = 'NOV'
        mes_anexo = "Novembro"
        return (mes, mes_anexo)
    elif number == 12:
        mes = 'DEZ'
        mes_anexo = "Dezembro"
        return (mes, mes_anexo)

#PDF
class PDF(FPDF):
    def header(self):
        # Logo(desloca horizontal, desloca vertical, tamanho)
        self.image(logo1, 10, 8, 60)
        #self.image('logo.png', 150, 8, 33)
	# Arial bold 15
        self.set_font('Arial', 'B', 30)
        # Move to the right and New Line
        self.ln(25)
	# Line break
        self.ln(20)

    # Page footer(Rodape)
    def footer(self):
        # Position at 1.5 cm from bottom
        self.set_y(-15)
        # Arial italic 8
        #self.set_font('Arial', 'I', 8)
        # Logo(desloca horizontal, desloca vertical, tamanho)
        self.cell(10)
        self.image(logo2, 10, 250, 180)

def export(num, year):
    subtitle = stitle.encode("latin-1","ignore")
    month = searchMonth(num)
    n = '{}'' - ''{}'.format(month['name'], year)
    name = n.encode("latin-1","ignore")
    abbre = '{}'.format(month['abbreviation'])
    dateInicio = '{}''{}'.format(year, month['dateI'])
    dateFim = '{}''{}'.format(year, month['dateF'])
    total1 = total(dateInicio, dateFim)
    nivel1 = n1(dateInicio, dateFim)
    nivel2 = n2(dateInicio, dateFim)
    atend = atendimento(dateInicio, dateFim)
    anexo = "Planilha com a listagem de chamados em anexo - RMSR_{}_{}.csv".format(abbre, year)
    total_cham = '{}'.format(total1)
    fech_nivel1 = 'Fechados 1 nivel: {}'.format(nivel1)
    fech_nivel2 = 'Fechados 2 nivel: {}'.format(nivel2)
    fech_atend = 'Em Atendimento : {}'.format(atend)
    pdf_name = '/var/www/html/Relatorio Issue Rede - {}_{}.pdf'.format(abbre, year)
    url = 'Relatorio Issue Rede - {}_{}.pdf'.format(abbre, year)
    #GERAR PDF
    pdf = PDF(orientation = 'P', unit = 'mm', format='A4')
    pdf.alias_nb_pages()
    pdf.add_page()
    #pdf.add_font('DejaVu', '', 'DejaVuSansCondensed.ttf', uni=True)
    #pdf.set_font('DejaVu', '', 14)
    # Title
    #Move to the right
    pdf.cell(30)
    pdf.cell(30, 40, title, 'C')
    pdf.ln(20)
    ##SubTitle
    pdf.set_font('Arial', 'B', 12)
    pdf.cell(12)
    pdf.cell(30, 50, subtitle,'C')
    pdf.ln(5)
    ##Mes
    pdf.cell(12)
    pdf.set_font('Arial', '', 12)
    pdf.cell(30, 50, name,'C')
    #Construindo as informacoes do pdf
    pdf.add_page()
    pdf.cell(12)
    pdf.set_font('Arial','',15)
    rel = 'Relatório de Atendimentos'
    rela = rel.encode("latin-1","ignore")
    pdf.write(15, rela)
    ##Contruindo a Tabela
    pdf.set_font('Arial','B',12)
    pdf.ln(30)
    pdf.cell(12)
    pdf.cell(150, 10, 'Quantitativo de Atendimentos', 1, 0, 'C')
    #Incidentes Abertos
    pdf.ln(10)
    pdf.cell(12)
    pdf.set_font('Arial','',12)
    pdf.cell(60, 10, 'Incidentes Abertos', 1, 0, 'C')
    pdf.set_font('Arial','',12)
    pdf.cell(90, 10, total_cham, 1, 0, 'L')
    #Incidentes Por status
    pdf.ln(10)
    pdf.cell(12)
    pdf.set_font('Arial','',12)
    pdf.cell(60, 30, 'Incidentes por Status', 1, 0, 'C')
    pdf.set_font('Arial','',12)
    pdf.cell(90, 10, fech_nivel1, 1, 0, 'L')
    pdf.ln(10)
    pdf.cell(72)
    pdf.cell(90, 10, fech_nivel2, 1, 0, 'L')
    pdf.ln(10)
    pdf.cell(72)
    pdf.cell(90, 10, fech_atend, 1, 0, 'L')
    #Outras Ocorrencias
    pdf.ln(10)
    pdf.cell(12)
    pdf.set_font('Arial','',12)
    pdf.cell(60, 10, 'Outras Ocorrencias', 1, 0, 'C')
    pdf.set_font('Arial','',12)
    pdf.cell(90, 10, 'Sem demais ocorrencias', 1, 0, 'L')
    ##Corpo
    pdf.ln(25)
    pdf.cell(12)
    pdf.set_font('Times','',12)
    con = 'Considerações sobre os atendimentos:'
    consid = con.encode("latin-1","ignore")
    pdf.write(15, consid)
    pdf.ln(7)
    pdf.cell(20)
    me = '> Todas as ocorrências atendidas dentro do SLA;'
    mensg = me.encode("latin-1","ignore")
    pdf.write(15,mensg)
    pdf.ln(7)
    pdf.cell(20)
    pdf.write(15,'>  O grande volume de chamados se deu devido, principalmente, a testes realizados pela')
    pdf.ln(7)
    pdf.cell(25)
    pdf.write(15,'equipe da Maxipago.')
    pdf.ln(15)
    pdf.cell(12)
    pdf.write(15,'Anexos:')
    pdf.ln(7)
    pdf.cell(20)
    pdf.write(15, anexo)
    pdf.output(pdf_name , 'F')
    return (url)
