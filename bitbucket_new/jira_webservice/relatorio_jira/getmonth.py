# -*- coding: utf-8 -*-
import sys
import json

def getM():
    month = [
    {
        'id': '01',
        'abbreviation':'JAN',
        'name':'Janeiro',
        'dateI':'-01-01',
        'dateF':'-01-31'
    },
    {
        'id': '02',
        'abbreviation':'FEV',
        'name':'Fevereiro',
        'dateI':'-02-01',
        'dateF':'-02-28'
    },
    {
        'id': '03',
        'abbreviation':'MAR',
        'name':'Março',
        'dateI':'-03-01',
        'dateF':'-03-31'
    },
    {
        'id': '04',
        'abbreviation':'ABR',
        'name':'Abril',
        'dateI':'-04-01',
        'dateF':'-04-30'
    },
    {
        'id': '05',
        'abbreviation':'MAI',
        'name':'Maio',
        'dateI':'-05-01',
        'dateF':'-05-31'
    },
    {
        'id': '06',
        'abbreviation':'JUN',
        'name':'Junho',
        'dateI':'-06-01',
        'dateF':'-06-30'
    },
    {
        'id': '07',
        'abbreviation':'JUL',
        'name':'Julho',
        'dateI':'-07-01',
        'dateF':'-07-31'
    },
    {
        'id': '08',
        'abbreviation':'AGO',
        'name':'Agosto',
        'dateI':'-08-01',
        'dateF':'-08-31'
    },
    {
        'id': '09',
        'abbreviation':'SET',
        'name':'Setembro',
        'dateI':'-09-01',
        'dateF':'-09-30'
    },
    {
        'id': '10',
        'abbreviation':'OUT',
        'name':'Outubro',
        'dateI':'-10-01',
        'dateF':'-10-31'
    },
    {
        'id': '11',
        'abbreviation':'NOV',
        'name':'Novembro',
        'dateI':'-11-01',
        'dateF':'-11-30'
    },
    {
        'id': '12',
        'abbreviation':'DEZ',
        'name':'Dezembro',
        'dateI':'-12-01',
        'dateF':'-12-31'
    },
    ]
    return month

def searchMonth(number):
    #global n
    mont = getM()
    for x in mont:
        if(x["id"] == number):
            return x
