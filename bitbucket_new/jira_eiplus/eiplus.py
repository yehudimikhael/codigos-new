#! -*-coding: utf-8 -*-
from jira import JIRA
import getpass

user = raw_input('Digite seu usuário: ')
password = getpass.getpass('Password: ')

options = {'server': 'https://concreteprodutos.atlassian.net'}
jira = JIRA(options, basic_auth=(user, password))

#Pegar o nome do usuario e colocar na assinatura
assignee = user.split('.')
assignee = assignee[0] + ' ' + assignee[1]
assignee = assignee.title()
#print(assignee)

#verificar o turno
while(True):
	print('\nQual turno você é: \n\t1-Manhã \n\t2-Tarde \n\t3-Noite \n\t4-Madrugada')
	op = input('Digite a opção: ')
	print(op)	
	if op == 1:
		turno = 'Validação de Navegação na Aplicação - Manhã - 06:00 às 12:15.'
		break
	elif op == 2:
		turno = 'Validação de Navegação na Aplicação - Tarde - 12:00 às 18:15.'
		break
	elif op == 3:
		turno = 'Validação de Navegação na Aplicação - Noite - 18:00 às 00:15.'
		break
	elif op == 4:
		turno = 'Validação de Navegação na Aplicação - Madrugada - 00:00 às 06:15.'
		break
	else:
		print('Digite as opções conforme informado: ')


desc = "Prezados,\n Estamos realizando o Logout/Logon e em seguida os testes de navegação pelos canais da aplicação.  \nAtt, \n{} \nManaged Services Team ".format(assignee)

#Comentario
#issue = jira.issue('EIPS-277')
#jira.add_comment(issue, 'Prezados, \nTestes realizados e a Aplicacao encontra-se operacional. \nAtt, \nYehudi Brito \nManaged Services Team')


#Fechar chamado
#https://concreteprodutos.atlassian.net/rest/api/2/statuscategory
#https://concreteprodutos.atlassian.net/rest/api/2/issue/EIPS-277/transitions
#transitions = jira.transitions(issue)
#[(t['id'], t['name']) for t in transitions]
#[(u'951', u'Escalated'), (u'761', u'Resolver esta pend\xeancia'), (u'901', u'Cancel request'), (u'871', u'Pending')]
#jira.transition_issue(issue, '761', resolution={'id': '10000'})
#issue.fields.resolution
#issue = jira.issue('EIPS-276')
#issue.fields.resolution276'
#<JIRA Resolution: name=u'Conclu\xedda', id=u'10000'>
#issue.fields.resolution
#Codigio final jira.transition_issue(issue, '761', resolution={'id': '10000'})


#print(desc)
#print(turno)
issue_dict = {
'project': {'id': 10302},
'summary': turno,
'description': desc,
'issuetype': {'name': 'Service Request'},

}
jira.create_issue(fields=issue_dict)
#issue = jira.issue('EIPS-277')
#print(issue)
