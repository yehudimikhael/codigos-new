#-*-coding: utf-8 -*-

#Importando Modulos
from flask import Flask
from flask_ask import Ask, statement, question, session

app = Flask(__name__)
ask = Ask(app, "/")

@ask.launch
def new_ask():
    welcome = 'Welcome Concrete Solutions'
    return question(welcome)

@ask.intent('User')
def get_user(user):
    hello = 'Hello %s'%user
    return question(hello)

if __name__=='__main__':
    app.run()