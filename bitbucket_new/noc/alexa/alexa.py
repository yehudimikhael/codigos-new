
#Importando Modulos
from flask import Flask, render_template
from flask_ask import Ask, statement, question, session
#import ushlex as shlex
#import subprocess
#import os
import logging

app = Flask(__name__)
ask = Ask(app, "/")

logging.getLogger("flask_ask").setLevel(logging.DEBUG)


@ask.launch
def new_ask():
    welcome = 'Welcome Concrete Solutions'
    return question(welcome)

@ask.intent('Temp')
def get_user():
    hello = 'Hello'
    return question(hello)

@ask.intent('NameNOC')
def user(name):
    ola = 'hello {}'.format(name)
    return question(ola)

if __name__=='__main__':
    app.run(debug=True)