#--coding: utf-8
import csv
from datetime import datetime
import os
import locale
from collections import OrderedDict
import xlsxwriter
import importlib.util
import sys
from get_comentario import get_comment
import xlrd
#from pip._internal import main 

#Verifica se o xlsx2csv esta instalado para a conversao do xlsx to csv
package_name = 'xlsx2csv'
package_name2 = 'xlsxwriter'
#spec = importlib.util.find_spec(package_name)
#if spec is None:
#    main(['install', package_name])



#path = input("Digite o nome do arquivo: ")
path = 'junho-2018.csv'

#Fazer com que o strptime aceitei o padrão Brasileiro referente a abreviação do Mês 
locale.setlocale(locale.LC_TIME, 'pt_BR.utf8')

#os.rename(path, path+'.bkp')
fread = open(path, 'r')
title = ['Tipo de item','Chave da item','Resolucao','ID da item', 'Prioridade', 'Status', 'Criado', 'Resumo', 'Campo personalizado (Meet SLA)']
array = []
check = 0 
w = 0
issue = ''
with open(path, 'r') as file:
    reader = csv.DictReader(file, fieldnames=title)
    #readerr = reader.readlines()
    #print(readerr)
    cont = 0   
    contt = 0
    for row in reader:
        # print(row.items())        
        # print(contt)
        # contt = contt + 1
        for k, v in row.items():
            if k == "Chave da item":
                if v == "Chave da item":
                    if check == 0:
                        print(check)
                        temp = [k, v]
                        array.append(temp)
                        #print("dentro-0")
                        check = 1
                        a = 'Resolucao'
                        b = 'Resolucao'
                        tempp = [a, b]
                        array.append(tempp) 
                        
                else:
                    #a = 'Resolucao'
                    #print(cont)
                    issue = get_comment(v)
            if k == "Resolucao":
                 if v == "":
                    t = 'Resolucao'
                    temp = [t, issue]
                    array.append(temp)              
            elif k == "Criado":
                if v == "Criado":
                    temp = [k, v]
                    array.append(temp)
                    x = 0
                else:
                    #print(v)
                    data, hrs, secs = v.split(' ')
                    convert = datetime.strptime(data, '%d/%b/%y')
                    #Separar a data da hora depois de converter
                    new_date, hrs = '{}'.format(convert).split(' ')
                    #Salvar os dados em um array de lista        
                    v = new_date
                    temp = [k, v]
                    array.append(temp)
            else:
                if v != "Chave da item":
                    temp = [k, v]
                    #print(temp)
                    array.append(temp)
            cont = cont + 1

name, exten = path.split('.')
#print(array)
#Gerar o Excel
file_name = name + '.xlsx'
workbook = xlsxwriter.Workbook(file_name)
worksheet = workbook.add_worksheet()
cont = 0
row = 0
col = 0 
tamanho = int(len(array))
while cont < tamanho:
    x = array[cont][0]
    y = array[cont][1]    
    print('{} - {}'.format(x, y))
    print("\b")
    if col < 8:        
        worksheet.write(row, col,y)
        col = col + 1
    else:
        row = row + 1
        col = 0
        
    cont += 1
workbook.close()

#Converter para csv
convert_file = 'xlsx2csv -s 0 {} {}'.format(file_name, name) 
os.system(convert_file)