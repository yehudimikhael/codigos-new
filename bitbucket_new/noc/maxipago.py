import telegram
import requests
import json
import datetime
import time
import os
import logging
import ushlex as shlex
import subprocess
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, RegexHandler, ConversationHandler

logging.basicConfig(level=logging.DEBUG,format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

updater = Updater('541113061:AAEdXIHCtZEkyyJLbusmWygVVe-1UZhwHYI')


url_api = "https://api.maxipago.net//UniversalAPI/postXML"
url_portal = "https://portal.maxipago.net/vpos/docs/maxipagologin.html"
url_elb = "http://elb-ap-2025226693.us-east-1.elb.amazonaws.com"
laten = "curl -s -w %{time_total} -o /dev/null "
command = './latencia.sh '
ok = "<Response [200]>"
def start(bot,update):
    id_user = update.message.chat_id
    user = str(id_user)
     
    #POST API
    api = requests.post(url_api)
    command = './latencia.sh ' + url_api
    subprocess.call(shlex.split(command))
    arq = open('latencia.txt', 'r' )
    contents_api = arq.read()
    arq.close()
    
    #GET ELB
    elb = requests.get(url_elb)
    command_elb = './latencia.sh ' + url_elb
    subprocess.call(shlex.split(command_elb))
    arq = open('latencia.txt', 'r' )
    contents_elb = arq.read()
    arq.close()

    #GET PORTAL
    portal = requests.get(url_portal)
    command_portal = './latencia.sh ' + url_portal
    subprocess.call(shlex.split(command_portal))
    arq = open('latencia.txt', 'r' )
    contents_portal = arq.read()
    arq.close()

    
    if ((str(api) == str(ok)) == True):
        api_text = '#### POST - API MaxiPago ####\n {} \n #### Latencia ###\n {}ms \n\n'.format(api, contents_api)
        text = api_text.encode('utf8')
        bot.sendMessage(update.message.chat_id, text)
    if ((str(elb) == str(ok)) == True):
        elb_text = '#### GET - ELB-AP ####\n {} \n #### Latencia ###\n {}ms \n \n'.format(elb, contents_elb)
        textelb = elb_text.encode('utf8')
        bot.sendMessage(update.message.chat_id, textelb)
    if ((str(portal) == str(ok)) == True):
        portal_text = '#### GET - PORTAL ####\n {} \n #### Latencia ###\n {}ms \n \n'.format(portal, contents_portal)
        textportal = portal_text.encode('utf8')
        bot.sendMessage(update.message.chat_id, textportal)
    else :
        print ("Fora")


updater.dispatcher.add_handler(CommandHandler('start', start))
updater.start_polling()
updater.idle()

